const dev = {
  s3: {
    REGION: "eu-west-2",
    BUCKET: "serverless-notes-prod-attachmentsbucket-1mapumkudjfsc"
  },
  apiGateway: {
    REGION: "eu-west-2",
    URL: "https://api.harrytang.com/notes"
  },
  cognito: {
    REGION: "eu-west-2",
    USER_POOL_ID: "eu-west-2_4GpBikHKp",
    APP_CLIENT_ID: "30u5gek3j58a6209lim1jule8i",
    IDENTITY_POOL_ID: "eu-west-2:9438d2a4-daf3-4e7e-b6af-9db8a2ccf45e"
  },
  STRIPE_KEY: "pk_test_qMnHjhqayDF3L3hAZJ1nwjwP"
};

const prod = {
  s3: {
    REGION: "eu-west-2",
    BUCKET: "serverless-notes-prod-attachmentsbucket-1mapumkudjfsc"
  },
  apiGateway: {
    REGION: "eu-west-2",
    URL: "https://api.harrytang.com/notes"
  },
  cognito: {
    REGION: "eu-west-2",
    USER_POOL_ID: "eu-west-2_4GpBikHKp",
    APP_CLIENT_ID: "30u5gek3j58a6209lim1jule8i",
    IDENTITY_POOL_ID: "eu-west-2:9438d2a4-daf3-4e7e-b6af-9db8a2ccf45e"
  },
  STRIPE_KEY: "pk_test_qMnHjhqayDF3L3hAZJ1nwjwP"
};

// Default to dev if not set
const config = process.env.REACT_APP_STAGE === 'prod'
  ? prod
  : dev;

export default {
  // Add common config values here
  MAX_ATTACHMENT_SIZE: 5000000,
  ...config
};